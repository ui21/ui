import "bootstrap/dist/css/bootstrap.css";
import React, { Component } from "react";
import { Container } from "react-bootstrap";
import ContentPage from "./components/ContentPage";
import NavBar from './components/NavBar'
import './app.css'

export default class App extends Component {
  render() {
    return (
      <div>
        <NavBar/>
        <Container>
          <ContentPage/>
        </Container>
      </div>
    );
  }
}
