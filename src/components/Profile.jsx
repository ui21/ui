import React from "react";
import "bootstrap/dist/css/bootstrap.css";

export default function Profile() {

  return (
    <div>
      <form>
        {/* icon */}
        <div className="mb-3">
          <h3>Person Info</h3>
        </div>
        <div className="row">
          <div className="col-lg-6 col-sm-12">
            {/* username */}
            <div className="mb-3">
              <label htmlFor="username" className="form-label">
                Username
              </label>
              <input
                type="text"
                className="form-control"
                id="usernameInput"
                aria-describedby="userlHelp"
                placeholder="Username"
                onChange={usernameValidation}
              />
              <p id="userHelp" className="form-text invalid-feedback"></p>
            </div>
            {/* email */}
            <div className="mb-3">
              <label htmlFor="emailInput" className="form-label">
                Email
              </label>
              <input
                type="email"
                className="form-control"
                id="emailInput"
                aria-describedby="emailHelp"
                placeholder="Example@gmail.com"
                onChange={emailValidation}
              />
              <p id="emailHelp" className="form-text invalid-feedback"></p>
            </div>
          </div>
          <div className="col-lg-6 col-sm-12">
            {/* Gender */}
            <label className="form-label">Gender</label>
            <div className="mb-3" id="gender">
              <div className="form-check form-check-inline">
                <input
                  className="form-check-input checked"
                  type="radio"
                  id="rd-male"
                  value="male"
                  name="rd-gender"
                  onChange={(e) => { }}
                  checked
                />
                <label className="form-check-label" htmlFor="rd-male">
                  male
                </label>
              </div>
              <div className="form-check form-check-inline">
                <input
                  className="form-check-input"
                  type="radio"
                  id="rd-female"
                  value="female"
                  name="rd-gender"
                />
                <label className="form-check-label" htmlFor="rd-female">
                  female
                </label>
              </div>
            </div>
            {/* Job */}
            <label className="form-label mt-2">Job</label>
            <div className="mb-3" id="job">
              <div className="form-check form-check-inline">
                <input
                  className="form-check-input"
                  type="checkbox"
                  id="chk-student"
                  value="student"
                  name="chk-job"
                  onChange={(e) => { }}
                />
                <label className="form-check-label" htmlFor="chk-student">
                  Student
                </label>
              </div>
              <div className="form-check form-check-inline">
                <input
                  className="form-check-input"
                  type="checkbox"
                  id="chk-teacher"
                  value="teacher"
                  name="chk-job"
                  onChange={(e) => { }}
                />
                <label className="form-check-label" htmlFor="chk-teacher">
                  Teacher
                </label>
              </div>
              <div className="form-check form-check-inline">
                <input
                  className="form-check-input"
                  type="checkbox"
                  id="chk-developer"
                  value="developer"
                  name="chk-job"
                  onChange={(e) => { }}
                />
                <label className="form-check-label" htmlFor="chk-developer">
                  Developer
                </label>
              </div>
            </div>
          </div>
        </div>
        <button type="button" className="btn btn-primary mb-3">
          Save
        </button> {' '}
        <button type="button" className="btn btn-danger mb-3">
          Cancel
        </button>
      </form>
    </div>
  );
}
