import React from "react";
import { Tabs, Tab, DropdownButton, Dropdown, Card, ButtonGroup } from "react-bootstrap";
import { FaEye, FaEdit, FaTrash } from "react-icons/fa";

export default function TableInfo() {
  return (
    <div>
      <h3 className="mt-5 mb-3">Table Account</h3>
      <Tabs defaultActiveKey="Table" id="uncontrolled-tab-example">
        <Tab eventKey="Table" title="Table">
          <table className="table" id="table-data">
            <thead>
              <tr className="bg-success text-light">
                <th scope="col" style={{ width: "5%" }}>#</th>
                <th scope="col" style={{ width: "15%" }}>Name</th>
                <th scope="col" style={{ width: "15%" }}>Email</th>
                <th scope="col" style={{ width: "10%" }}>Gender</th>
                <th scope="col" style={{ width: "10%" }}>Job</th>
                <th scope="col" style={{ width: "15%" }}>Created at</th>
                <th scope="col" style={{ width: "15%" }}>Updated at</th>
                <th scope="col" style={{ width: "20%" }}>Action</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>
                  <div id="table-button">
                    {/* btn show */}
                    <button type="button" class="btn btn-primary mb-2 btn-view">
                      <FaEye />
                    </button>
                    {/* btn update */}
                    <button type="button" class="btn btn-light mb-2 btn-update">
                      <FaEdit />
                    </button>
                    {/* btn delete */}
                    <button type="button" class="btn btn-danger mb-2">
                      <FaTrash />
                    </button>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </Tab>
        <Tab eventKey="Card" title="Card">
          <Card border="primary" style={{ width: '15rem' }} className="mt-4 mb-3">
            <Card.Header className="text-center">
              <DropdownButton as={ButtonGroup} variant="success" title="Action" id="bg-nested-dropdown" className="ml-5 btn-action">
                <Dropdown.Item eventKey="1"><FaEye/> View</Dropdown.Item>
                <Dropdown.Item eventKey="2"><FaEdit/> Update</Dropdown.Item>
                <Dropdown.Item eventKey="3"><FaTrash/> Delete</Dropdown.Item>
              </DropdownButton>
            </Card.Header>
            <Card.Body>
              <Card.Title>Primary Card Title</Card.Title>
              <Card.Text>
                Some quick example text to build on the card title
              </Card.Text>
            </Card.Body>
            <Card.Footer>Footer</Card.Footer>
          </Card>
        </Tab>
      </Tabs>
    </div>
  );
}
