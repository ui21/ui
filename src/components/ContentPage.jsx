import React from "react";
import Profile from "./Profile";
import TableInfo from "./TableInfo";

export default function ContentPage() {
  return (
    <div>
          <Profile/>
          <TableInfo/>
    </div>
  );
}
